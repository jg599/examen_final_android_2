package com.example.yony.actividad8.adapters;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.yony.actividad8.fragmets.GGMapaFragment;
import com.example.yony.actividad8.fragmets.MessagesListFragment;
import com.example.yony.actividad8.fragmets.UsersFragment;


/**
 * Created by Yony on 21/02/16.
 */
public class TabsPagerAdapter extends FragmentPagerAdapter{

    public UsersFragment usersFragment;
    public MessagesListFragment messagesListFragment;
    public GGMapaFragment GGMapaFragment;

    public TabsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        //return TabPagerFragment.newInstance(position + 1);
        if(position==0){
            usersFragment=UsersFragment.newInstance(position+1);
            return usersFragment;
        }
        else if(position==1){
            messagesListFragment=MessagesListFragment.newInstance(position+1);
            return messagesListFragment;
        }
        else if(position==2){
            GGMapaFragment = GGMapaFragment.newInstance(position + 1);
            return GGMapaFragment;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "TAB " + (position + 1);
    }
}
