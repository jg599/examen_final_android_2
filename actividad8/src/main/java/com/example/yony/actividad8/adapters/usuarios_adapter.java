package com.example.yony.actividad8.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.yony.actividad8.R;
import com.quickblox.users.model.QBUser;

import java.util.ArrayList;

/**
 * Created by jose.mansilla on 26/02/2016.
 */
public class usuarios_adapter extends BaseAdapter {
    /*
    Este adaptador es el que servirá para pasar los QBusers a nuestro listview
     */

    ArrayList myList = new ArrayList();
    LayoutInflater inflater;
    Context context;
    Activity act;
    ArrayList<QBUser> usr;


    public usuarios_adapter(Activity act,ArrayList<QBUser> usr ) {
        this.act= act;
        this.usr=usr;
  //      this.context = context;
       inflater = LayoutInflater.from(act);
    }
    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

View v = convertView;

inflater=act.getLayoutInflater();




        v = inflater.inflate(R.layout.inflate_lista, parent, false);
        TextView caja = (TextView)v.findViewById(R.id.nomb_text);
        caja.setText(usr.get(position).getLogin());

        return null;
    }
}
