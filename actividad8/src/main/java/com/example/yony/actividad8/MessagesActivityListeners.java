package com.example.yony.actividad8;

import android.util.Log;

import com.quickblox.core.helper.StringifyArrayList;
import com.quickblox.customobjects.model.QBCustomObject;
import com.quickblox.users.model.QBUser;

import java.util.ArrayList;

import gebulot.pmdmlib.QbAdmin.QbAdminListener;

/**
 * Created by Yony on 21/02/16.
 */
public class MessagesActivityListeners implements QbAdminListener{

    public MessagesActivity ma;
    public MessagesActivityListeners(MessagesActivity ma){
        this.ma=ma;
        DataHolder.instance.qbAdmin.setQbAdminListener(this);

        descargarListaUsuarios();
    }

    @Override
    public void sessionCreated(boolean blCreated) {

    }

    @Override
    public void loginSuccess(boolean blLogin) {

    }

    @Override
    public void registerSuccess(boolean blLogin) {

    }

    @Override
    public void getTableSuccess(long timeID, ArrayList<QBCustomObject> customObjects) {

    }

    public void descargarListaUsuarios(){
        ArrayList<String> arrayList=new ArrayList<String>();
        arrayList.add("damp");
        DataHolder.instance.qbAdmin.getUsersList(arrayList);
    }

    @Override
    public void listaUsuariosRecibidos(ArrayList<QBUser> qbUsers) {
        //Log.v("MAL","!!!!!!!! "+qbUsers);
        ma.tabsPagerAdapter.usersFragment.setQbUsers(qbUsers);
        StringifyArrayList<String> st=new StringifyArrayList<String>();
        st.add("dampprof");
        DataHolder.instance.enviarMensajePush("Prueba",10.0,11.0,null,st);
    }
}
