package com.example.yony.actividad8.fragmets;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.yony.actividad8.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class MessagesListFragment extends Fragment {


    public MessagesListFragment() {
        // Required empty public constructor
    }

    public static MessagesListFragment newInstance(int page) {
        MessagesListFragment fragment = new MessagesListFragment();
        Bundle args = new Bundle();
        args.putInt("numero_tab", page);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_messages_list, container, false);
        TextView txt = (TextView) rootView.findViewById(R.id.page_number_label);
        int page = getArguments().getInt("numero_tab", -1);
        txt.setText(String.format("Fragmento Mensajes Tab %d", page));
        getChildFragmentManager();
        return rootView;
    }

}
