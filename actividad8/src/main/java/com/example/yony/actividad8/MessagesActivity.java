package com.example.yony.actividad8;

import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.example.yony.actividad8.adapters.TabsPagerAdapter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

public class MessagesActivity extends AppCompatActivity {

    TabLayout tabLayout;
    MessagesActivityListeners messagesActivityListeners;
    TabsPagerAdapter tabsPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages);
        messagesActivityListeners=new MessagesActivityListeners(this);
        initViews();

    }

    public void initViews(){
        /*tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.addTab(tabLayout.newTab().setText("Tab 1"));
        tabLayout.addTab(tabLayout.newTab().setText("Tab 2"));
        tabLayout.addTab(tabLayout.newTab().setText("Tab 3"));*/

        /*TabsPagerAdapter adapter = new TabsPagerAdapter(getSupportFragmentManager());
        ViewPager pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(adapter);*/

        TabLayout tabs = (TabLayout) findViewById(R.id.tabs);
        ViewPager pager = (ViewPager) findViewById(R.id.pager);
        tabsPagerAdapter = new TabsPagerAdapter(getSupportFragmentManager());

        pager.setAdapter(tabsPagerAdapter);
        tabs.setupWithViewPager(pager);
    }

    public void cargarFicheroconDatos() {

        try {
            File f = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "mensajes.pmdm");
            FileInputStream fis = new FileInputStream(f);
            InputStreamReader isr = new InputStreamReader(fis, "UTF-8");
            BufferedReader bf = new BufferedReader(isr);
            String sLine = null;
            while ((sLine = bf.readLine()) != null) {
                String[] sTokens = sLine.split("|");
                addMessageToList(sTokens[0], sTokens[1], sTokens[2]);
            }
            bf.close();
            isr.close();
            fis.close();
        } catch (Exception e) {

        }


    }

    public void guardarEnFichero() {

        try{
            File f = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "mensajes.pmdm");
            FileOutputStream fis = new FileOutputStream(f);
            OutputStreamWriter osr = new OutputStreamWriter(fis, "UTF-8");
            PrintWriter pw = new PrintWriter(osr);


            //  for (int i=0;i<msgs.size();i++){
            //  pw.println(msgs.get(i).msg + "|" + msgs.get(i).cid+"|"+msgs.get(i).user);
            //    pw.println(msgs.get(i));
            // }
            pw.close();

            fis.close();

            osr.close();

        }catch (Exception e){

        }
    }
    public void addMessageToList(String sToken, String token, String s){

    }
}




