package com.example.yony.actividad8.fragmets;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.example.yony.actividad8.R;
import com.example.yony.actividad8.adapters.usuarios_adapter;
import com.quickblox.users.model.QBUser;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class UsersFragment extends Fragment {


    private ArrayList<QBUser> qbUsers;
    private ListView lv;
    ArrayList<ArrayList<QBUser>> lista_usuarios =new ArrayList<>();
    public UsersFragment() {
        // Required empty public constructor
    }



    public static UsersFragment newInstance(int page) {
        UsersFragment fragment = new UsersFragment();
        Bundle args = new Bundle();
        args.putInt("numero_tab", page);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_users_fragments, container, false);
        TextView txt = (TextView) rootView.findViewById(R.id.page_number_label);
        int page = getArguments().getInt("numero_tab", -1);
//        txt.setText(String.format("Fragmento Users Tab %d", page));


        lv = (ListView)rootView.findViewById(R.id.listView_users);





        return rootView;
    }

    public ArrayList<QBUser> getQbUsers() {
        return qbUsers;
    }

    public void setQbUsers(ArrayList<QBUser> qbUsers) {
        this.qbUsers = qbUsers;
        usuarios_adapter adaptador =new usuarios_adapter(getActivity(),qbUsers);
       // usuarios_adapter<QBUser> adaptador = new usuarios_adapter<QBUser>(getActivity(), android.R.layout.simple_list_item_1, lista_usuarios);
        lv.setAdapter(adaptador);


    }
}
