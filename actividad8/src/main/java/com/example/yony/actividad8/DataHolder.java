package com.example.yony.actividad8;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.core.helper.StringifyArrayList;
import com.quickblox.messages.QBMessages;
import com.quickblox.messages.model.QBEnvironment;
import com.quickblox.messages.model.QBEvent;
import com.quickblox.messages.model.QBNotificationType;

import org.json.JSONObject;

import java.util.List;
import java.util.Random;

import gebulot.pmdmlib.PushNotificationAdmin.Consts;
import gebulot.pmdmlib.PushNotificationAdmin.PushNotificationAdmin;
import gebulot.pmdmlib.PushNotificationAdmin.PushNotificationsAdminListener;
import gebulot.pmdmlib.QbAdmin.QbAdmin;

/**
 * Created by Yony on 20/01/2016.
 */
public class DataHolder implements PushNotificationsAdminListener{

    public final static DataHolder instance=new DataHolder();
    public final String TAG="DataHolder";


    public QbAdmin qbAdmin;
    public PushNotificationAdmin pushNotificationAdmin;

    public DataHolder(){

    }

    public void initQbAdmin(Context context){
        qbAdmin=new QbAdmin(context,"29341", "wxfT9He8dhp2JN2", "sTeZqDvpbSNeX7K");
    }

    public void initPushNotificationsAdmin(Activity activity,String aid){
        pushNotificationAdmin=new PushNotificationAdmin(activity,aid);
        pushNotificationAdmin.addListener(this);


        LocalBroadcastManager.getInstance(activity).registerReceiver(mPushReceiver,
                new IntentFilter(Consts.NEW_PUSH_EVENT));
    }

    public void enviarMensajePush(String texto, double lat, double lon,StringifyArrayList<Integer> userIds,
                                  StringifyArrayList<String> tags ){
        // Send Push: create QuickBlox Push Notification Event
        QBEvent qbEvent = new QBEvent();
        qbEvent.setNotificationType(QBNotificationType.PUSH);
        qbEvent.setEnvironment(QBEnvironment.DEVELOPMENT);

        if(userIds!=null)qbEvent.setUserIds(userIds);
        if(tags!=null)qbEvent.setUserTagsAny(tags);

        // generic push - will be delivered to all platforms (Android, iOS, WP, Blackberry..)
        //qbEvent.setMessage(messageOutEditText.getText().toString());

        JSONObject jsonMensaje = new JSONObject();
        try {
            //jsonMensaje = new JSONObject();
            jsonMensaje.put("idUsuario", "000000");
            jsonMensaje.put("nombreUsuario", "XXXXXXXXXXX");
            jsonMensaje.put("latitude", ""+(lat+(Math.random()-0.5)));
            jsonMensaje.put("longitude", ""+(lon+(Math.random()-0.5)));
            jsonMensaje.put("youtube", "XXXXXXXXXX");
            jsonMensaje.put("message", texto);
            jsonMensaje.put("tituloMensaje","");
        }catch (Exception ex){
            ex.printStackTrace();
        }
        String mensajeJson = jsonMensaje.toString();
        qbEvent.setMessage(mensajeJson);

        QBMessages.createEvent(qbEvent, new QBEntityCallbackImpl<QBEvent>() {
            @Override
            public void onSuccess(QBEvent qbEvent, Bundle bundle) {

            }

            @Override
            public void onError(List<String> strings) {

            }
        });
    }

    @Override
    public void pushNotificationsRegistered(boolean blRegistered) {

    }

    // ESTE ES EL ULTIMO PASO QUE HARA EL MENSAJE RECIBIDO. AQUI ES DONDE EJECUTAMOS LO QUE NOS INTERESE EJECUTAR
    //AL RECIBIR UN MENSAJE. EN CASO DE RECIBIR EL MENSAJE CUANDO ESTAMOS DENTRO DE LA APP, O SI ESTAMOS FUERA DE LA APP
    //AQUI ES DONDE LLEGA EL MENSAJE. TODAS LAS ACCIONES QUE HAGAMOS CON EL MENSAJE SE HARAN AQUI.
    //
    private BroadcastReceiver mPushReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            // Get extra data included in the Intent
            String message = intent.getStringExtra(Consts.EXTRA_MESSAGE);
            String qbcid = intent.getStringExtra("QBCID");
            String idUsuario=intent.getStringExtra("idUsuario");
            String youtube=intent.getStringExtra("youtube");
            String longitude=intent.getStringExtra("longitude");
            String latitude=intent.getStringExtra("latitude");
            String nombreUsuario=intent.getStringExtra("nombreUsuario");
            Log.v(TAG, "Receiving event " + Consts.NEW_PUSH_EVENT + " with data: " + message+"  ----   "+idUsuario
            +"  ----  "+youtube+"  ----  "+longitude+" ---- "+latitude+" ----- "+nombreUsuario);

            //AQUI INSERTAREMOS EL CODIGO QUE EJECUTAREMOS CUANDO LLEGUE EL MENSAJE.
        }
    };
}
