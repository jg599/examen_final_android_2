package com.example.yony.actividad8.fragmets;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.yony.actividad8.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class GGMapaFragment extends Fragment implements OnMapReadyCallback ,YouTubePlayer.OnInitializedListener{

    private MapView mMapView;
    private GoogleMap mMap;
    YouTubePlayerFragment myYouTubePlayerFragment;
    private YouTubePlayer youTubePlayer;
    /*
    Developer_KEY sera la api_key que usara nuestro youtubeplayer
     */
    public static final String DEVELOPER_KEY = "AIzaSyDmw3I9KD66HselWa2d9E7p0IdqIcO1mqQ";
    private static final int RECOVERY_DIALOG_REQUEST = 1;
    /*
    Esta es la key asociada con el video , permite que se visualiza el video seleccionado
     */
    private static final String VIDEO_ID = "mMWBh0B9F0M";
    public GGMapaFragment() {
        // Required empty public constructor
    }

    public static GGMapaFragment newInstance(int page) {
        GGMapaFragment fragment = new GGMapaFragment();
        Bundle args = new Bundle();
        args.putInt("numero_tab", page);
        fragment.setArguments(args);

        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_gg_mapa, container, false);
        TextView txt = (TextView) rootView.findViewById(R.id.page_number_label);
        int page = getArguments().getInt("numero_tab", -1);
        txt.setText(String.format("Fragmento Mapa Tab %d", page));
        initMapFragment();
        initYoutubeFragment();
        /*
        esto no es compatible con fragment.ip.v4 los sustituimos por el menoto initYoutubeFragment
         */
      //  myYouTubePlayerFragment = (YouTubePlayerFragment)getChildFragmentManager().findFragmentById(R.id.YouTubePlayerFragment);
       // myYouTubePlayerFragment.initialize(DEVELOPER_KEY, this);
        return rootView;
    }

    public void initMapFragment(){
        FragmentManager fm = getChildFragmentManager();
        SupportMapFragment fragment = (SupportMapFragment) fm.findFragmentById(R.id.ggmapfragment);
        if (fragment == null) {
            fragment = SupportMapFragment.newInstance();
            fm.beginTransaction().replace(R.id.ggmapfragment, fragment).commit();
        }
        fragment.getMapAsync(this);
    }
/*
inicializo youtube
 */
    public void initYoutubeFragment(){
        FragmentManager fm = getChildFragmentManager();
        YouTubePlayerSupportFragment fragment = (YouTubePlayerSupportFragment) fm.findFragmentById(R.id.YouTubePlayerSupportFragment);
        if (fragment == null) {
            fragment = YouTubePlayerSupportFragment.newInstance();

            fm.beginTransaction().replace(R.id.YouTubePlayerSupportFragment, fragment).commit();

        }
        fragment.initialize(DEVELOPER_KEY, this);
    }
/*
Cuando el mapa esta listo se crea un marcador en sidney , con la latitud y longitud indicadas , despues saco el onmarkerclicklistener
y le añado la funcionalidad cueVideo que permite comenzar la reproduccion del video
 */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            /*
            cuando se pincha en el markador comienza el video de youtube
             */
            @Override
            public boolean onMarkerClick(Marker marker) {
                youTubePlayer.cueVideo(VIDEO_ID);
                //    getSupportFragmentManager().beginTransaction().replace(R.id.content,fragment_you).commit();
                return true;
            }
        });

    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {

    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

    }
}
